import { useState, useEffect, useRef, memo } from 'react'
import Box from '@mui/material/Box'
import LoadingButton from '@mui/lab/LoadingButton';
import { Button, Card, CardContent, Tooltip, Slider, Input, Collapse, Snackbar, FormControlLabel, Checkbox, Switch } from '@mui/material'
import { Container, CssBaseline, TextField, Typography, Grid, Divider } from '@mui/material'
import { Niivue, NVImage } from '@niivue/niivue'
import { io } from "socket.io-client";
import "./App.css"

// get web socket connection info from url before rendering anything
let urlParams = new URLSearchParams(window.location.search)
let host = urlParams.get('host')
let socketServerPort = urlParams.get('socketServerPort')
let fileServerPort = urlParams.get('fileServerPort')
let useShortDisplay = urlParams.get('shortNames') // can be set from global FSL settings on user's machine
console.log(host, socketServerPort, fileServerPort)

// different socket clients for different widgets
const inFileSocket = io(`ws://${host}:${socketServerPort}`)
const runSocket = io(`ws://${host}:${socketServerPort}`)
const fsleyesSocket = io(`ws://${host}:${socketServerPort}`)

const nv = new Niivue({clipPlaneColor:[0, 0, 0, 0]})

async function addNiiVueImages(urls, colors) {
	for (let i=0; i < urls.length; i++){
		let img = await NVImage.loadFromUrl(urls[i],'',colors[i])
		nv.addVolume(img)
	}
}

const NiiVue = ({url}) => {
	// use url string (primative) since object equality will not work with effects
	const canvas = useRef()
	useEffect(() => {
		nv.attachToCanvas(canvas.current)
		nv.loadVolumes([{url:url}])
	}, [url])

	return (
		<Grid container item xs={12} m={2} alignItems='center' justifyContent='center' spacing={0} direction='row'>
			<canvas ref={canvas} height={200} width={640}></canvas>
	</Grid>
	)
}

function InputField({text, updateOptsValue}) {
	useEffect(()=>{
		inFileSocket.on('files', (data) => {
			console.log(data)
			updateOptsValue('input', data[0])
		})
	}, [])

	return (
		<Grid container item xs={12} alignItems='center' spacing={0}>
			<Grid item xs={9}>
				<TextField 
					fullWidth 
					size="small" 
					label='input file' 
					onInput={(e) => {updateOptsValue('input', e.target.value)}}
					value={text}>
				</TextField>
			</Grid>
			<Grid item xs={3} sx={{justifyContent: 'flex-end'}}>
				<Button 
					variant='contained' 
					style={{margin:0, marginLeft: 12}}
					onClick={()=>{inFileSocket.emit('files')}}
				>
					Choose
				</Button>
			</Grid>
		</Grid>
	)
}

function Title({text}) {
	return (
		<Typography variant='h5' sx={{margin: '20px'}}>
			{text}
		</Typography>
	)
}

function OutputField({text, updateOptsValue}) {
	return (
		<Grid container item xs={12} alignItems='center' spacing={0}>
			<Grid item xs={9}>
				<TextField 
					fullWidth 
					size="small" 
					label='output base name'
					onInput={(e) => {updateOptsValue('-o', e.target.value)}}
					value={text}
				>
				</TextField>
			</Grid>
		</Grid>
	)
}


function ActionButtons({handleMoreOptions, commandString, isRunning, setIsRunning, options}) {
	return (
		<Grid container item xs={12} alignItems='center' justifyContent='center' spacing={0} direction='row'>
			<LoadingButton
				onClick={()=>{runSocket.emit('run', {'run': commandString}); setIsRunning(true)}}
        loading={isRunning}
        loadingPosition="end"
        variant="contained"
				style={{margin:0}}
      >
        Run
      </LoadingButton>
			<Button style={{margin:0}} onClick={handleMoreOptions}>more options</Button>	
		</Grid>
	)
}

function OptionCheckBox({optionValue, updateOptsValue, label}) {
	return (
		<FormControlLabel 
			control={<Checkbox checked={optionValue} onChange={updateOptsValue}/>}
			label={label}
		/>
	)
}

function OptionsContainer({moreOptions, options, updateOptsValue}) {
	return (
		<Collapse in={moreOptions} timeout="auto" unmountOnExit>
			<Grid container item xs={12} m={2} spacing={0} direction='column'>
				<FormControlLabel
					control={<Input type='number' value={options['-n']} onInput={(e)=>{updateOptsValue('-n', e.target.value)}}></Input>}
					label={'number of tissue classes to segment'}
				/>
				<OptionCheckBox
					optionValue={options['--nopve']}
					label={'do not output partial volume maps'}
					updateOptsValue={() => {updateOptsValue('--nopve', !options['--nopve'])}}
				/>
				<OptionCheckBox
					optionValue={options['-g']}
					label={'save binary segmentations for each tissue class'}
					updateOptsValue={() => {updateOptsValue('-g', !options['-g'])}}
				/>
				<OptionCheckBox
					optionValue={options['-B']}
					label={'save bias corrected input'}
					updateOptsValue={() => {updateOptsValue('-B', !options['-B'])}}
				/>
				<OptionCheckBox
					optionValue={options['-b']}
					label={'save estimated bias field'}
					updateOptsValue={() => {updateOptsValue('-b', !options['-b'])}}
				/>
				
			</Grid>
		</Collapse>
	)
}

function CommandStringPreview({commandString}) {
	return (
		<Grid 
			container 
			item 
			xs={12}
			alignItems='center'
			justifyContent='center' 
			spacing={0}
			direction='row'
		>
			<Typography sx={{fontFamily: 'Monospace'}}>
				{`${commandString}`}
			</Typography>
		</Grid>
	)
}

function OpenInFsleyes({options, getOutputName}) {
	let fsleyesString = `fsleyes ${options['input']} ${getOutputName(options['input'])}*pve_*`
	return (
		<Grid 
			container 
			item 
			xs={12}
			alignItems='left'
			spacing={0}
			direction='row'
		>
			<Tooltip title={fsleyesString}>
				<Button style={{margin:0, textTransform: 'none'}} onClick={() => {fsleyesSocket.emit('run', {'run': fsleyesString})}}>open FSLeyes</Button>
			</Tooltip>
		</Grid>

	)
}

export default function Fast() {
	const title = "FAST"
	// order matters. command string created from key order
  const defaultOpts = {
		'-o': 				'fast',
		'-n': 				3,
		'-B': 				false,
		'-b': 				false,
		'-g': 				false,
		'--nopve': 		false,
		'input': 		'input',
	}
	const [opts, setOpts] = useState(defaultOpts)
	// the boolean variable to show or hide the snackbar
	const [showSnackBar, setShowSnackBar] = useState(false)
	// the message, and message setter for the snackbar
	const [snackBarMessage, setSnackBarMessage] = useState('')
	const [moreOptions, setMoreOptions] = useState(false)
	const [commandString, setCommandString] = useState('')
	const [isRunning, setIsRunning] = useState(false)
	const [niivueImage, setNiivueImage] = useState('')
	
	if (host === null || socketServerPort === null || fileServerPort === null){
		setSnackBarMessage('unable to contact backend application')
	}

	inFileSocket.on('files', (data) => {
		updateOptsValue('input', data[0] ? data[0] : '')
		if (data[0] !== '') {
			setNiivueImage(`http://${host}:${fileServerPort}/file/?filename=${data[0]}`)
		}
	})

	runSocket.on('run', (data) => {
		console.log('run', data) 
		const availableColors = [
			'red', 
			'blue',
			'green',
			'cividis',
			'cool',
			'magma',
			'mako',
			'plasma',
			'rocket',
			'redyell',
			'winter'
		]
		let images = []
		for (let i=0; i < opts['-n']; i++){
			images.push(
				{
					url:`http://${host}:${fileServerPort}/file/?filename=${getOutputName(opts['input'])}_pve_${i}.nii.gz`,
					colorMap: availableColors[i]
				}
			)
		}
		if (isRunning){
			nv.loadVolumes(images)
			//addNiiVueImages(urls, colors)
		}
		setIsRunning(false)
	})

	function handleSnackBarClose() {
		setShowSnackBar(false)
	}

	function showSnackBarIfMsg() {
		if (snackBarMessage !== '') {
			setShowSnackBar(true)
		}
	}
	// whenever the snackbar message changes, run the effect (e.g. show the snackbar to the user when the message is updated)
	useEffect(() => {showSnackBarIfMsg()}, [snackBarMessage])
	useEffect(() => {updateCommandString()}, [opts])
	function getOutputName(fileName) {
		if (fileName === '' || typeof fileName === 'undefined') {
			return ''
		}
		let path = fileName.substring(0,fileName.lastIndexOf('/'))
		return `${path}/${opts['-o']}`
		
	}

	function shortenFileName(name) {
		if (useShortDisplay) {
			return name.split('/').pop() //assumes mac or linux for now...
		} else {
			return name
		}
	}

	function updateCommandString() {
		let command = 'fast '
		for (let [key, value] of Object.entries({...opts})) {
			if (value !== null) {
				if (value === false){
					continue
				} else if (value === true) {
					// if true the just pass the key to set boolean bet values
					value = ''	
				}
				command += `${(key=='input') ? '': key} ${(key === '-o') ? getOutputName(opts['input']) : value} `
			}
		}
		setCommandString(command)
	}


	function handleMoreOptions() {
		setMoreOptions(!moreOptions)
	}


	function updateOptsValue(option, value) {
		setOpts(defaultOpts => ({
			...defaultOpts,
			...{[option]: value}
		}))
		if (option === 'input') {
			//setOutputName(value)
			/*
			setOpts(defaultOpts => ({
				...defaultOpts,
				...{'-o': setOutputName(value)}
			}))
			*/
		}
	}

  return (
		<Container component="main" style={{height: '100%'}}>
			<CssBaseline enableColorScheme />
			<Box sx={{
				display: 'flex',
				flexDirection: 'column',
				alignItems: 'center',
				height: '100%'
				}}>
				<Title text='FAST' />
				<Grid container spacing={2}>
					<InputField
						updateOptsValue={updateOptsValue}
						text={opts['input']}
					/>
					<OutputField 
						updateOptsValue={updateOptsValue}
						text={opts['-o']}
					/>
					<ActionButtons
						handleMoreOptions={handleMoreOptions}
						commandString={commandString}
						isRunning={isRunning}
						setIsRunning={setIsRunning}
						options={opts}
					/>
					<OptionsContainer
						moreOptions={moreOptions}
						options={opts}
						updateOptsValue={updateOptsValue}
					/>
					<CommandStringPreview commandString={commandString}/>
					<OpenInFsleyes options={opts} getOutputName={getOutputName} />
					<NiiVue url={niivueImage}/>
					<Snackbar
						anchorOrigin={{horizontal:'center', vertical:'bottom'}}
						open={showSnackBar}
						onClose={handleSnackBarClose}
						message={snackBarMessage}
						key='betSnackBar'
						autoHideDuration={5000}
					/>
				</Grid>
			</Box>
		</Container>
  )
}
